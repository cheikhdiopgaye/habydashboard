export interface Alert {
  title: String;
  type: String;
  time: number;
  body: string;
}
