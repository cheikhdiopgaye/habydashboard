import { Injectable } from '@angular/core';
import {Alert} from '../interfaces/alert';
import { Subject } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AlertService {
  alertSintings$= new Subject<Alert>()

  constructor() { }
  create(
    title: string, type: string, time: number, body: string
  ){
    this.alertSintings$.next({
      title,
      type,
      time,
      body
    });
  }
}
