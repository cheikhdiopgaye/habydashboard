import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MatCard } from '@angular/material';

import { NotificationsService } from '../services/notifications.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';
interface Profil {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-newadmin',
  templateUrl: './newadmin.component.html',
  styleUrls: ['./newadmin.component.css']
})
export class NewadminComponent implements OnInit {

  profil: Profil[] = [
    {value: 'ADMINISTRATEUR', viewValue: 'Administrateur'},
    {value: 'RESPONSABLE', viewValue: 'Responsable'},
    {value: 'MANAGER', viewValue: 'Manager'}
  ];
  isSubmitted = false;
  submitForm: FormGroup;
  options = [{ value: "This is value 1", checked: true }];
  statuses = ["control"];

  // name = "Angular";//
  fileToUpload: any;
  imageUrl: any;



  constructor(private service: UtilisateurService,
    public formBuilder: FormBuilder ,
    private notificationService: NotificationsService,
    public dialogRef: MatDialogRef<NewadminComponent>) { }

  ngOnInit() {
    this.service.getListadmin();
    this.submitForm = this.formBuilder.group({
      profil: ['', [Validators.required]],
      username: ['', [Validators.required, Validators.minLength(5)]],
      prenom: ['',  [Validators.required, Validators.minLength(2)]],
      nom: ['',  [Validators.required]],
      telephone: ['',  [Validators.required, Validators.minLength(9)]],
      password: ['',  [Validators.required, Validators.minLength(6)]],
      confirmepassword: ['',  [Validators.required, Validators.minLength(6)]]
    })
  }
  initializeFormGroup() {
    this.submitForm.setValue({
      prenom: '',
      nom: '',
      username: '',
      profil: '',
      telephone: '',
      password: '',
      confirmepassword: ''
    });
  }
  onClear() {
    this.submitForm.reset();
    this.initializeFormGroup();
    this.notificationService.success(':: Submitted successfully');
  }
  get errorControl() {
    return this.submitForm.controls;
  }
  onClose() {
    this.submitForm.reset();
    this.initializeFormGroup();
    this.dialogRef.close();
  }
  onFileSelect(event) {
    if (event.target.files.length > 0) {
    const file = event.target.files[0];
     this.submitForm.get('photo').setValue(file);
   }
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.submitForm.valid) {
      console.log('validation erreur');
      } else {

        this.service.NewAdmin(this.submitForm.value).subscribe(

          res => {
            console.log(res);
          },
          err => {
            console.log(err);
          }
        );
        /* this.service.update(this.submitForm.value); */
        this.submitForm.reset();
        this.initializeFormGroup();
        this.notificationService.success(':: Submitted successfully');
    }
    
  }
}
