import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { NotificationsService } from '../services/notifications.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';

@Component({
  selector: 'app-newclient',
  templateUrl: './newclient.component.html',
  styleUrls: ['./newclient.component.css']
})
export class NewclientComponent implements OnInit {
  
  submitForm: FormGroup;
  isSubmitted = false;
  defaultDate = "1987-06-30";
  constructor(private service: UtilisateurService,
    public formBuilder: FormBuilder ,
    private notificationService: NotificationsService,
    public dialogRef: MatDialogRef<NewclientComponent>) { }

  ngOnInit() {
    this.service.getListuser(); 
    this.submitForm = this.formBuilder.group({
      civilite: ['', [Validators.required]],
      email: ['', [Validators.minLength(2), Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      nomcomplet: ['',  [Validators.required, Validators.minLength(2), Validators.pattern(/[a-z-A-Z]/)]],
      mobile: ['', [Validators.required, Validators.minLength(2), Validators.pattern(/[0-9]/)]],
      adresse: ['',  [Validators.required, Validators.minLength(3)]],
      datenaisse: [this.defaultDate],
      commentaire: ['',  [ Validators.minLength(5)]]
    })
  }
  initializeFormGroup() {
    this.submitForm.setValue({
      nomcomplet: '',
      email: '',
      mobile: '',
      adresse: '',
      civilite: '',
      datenaisse: '',
      commentaire: ''
    });
  }
  onClear() {
    this.submitForm.reset();
    this.initializeFormGroup();
    this.notificationService.success(':: Nouveau client ajouté.');
  }
  onClose() {
    this.submitForm.reset();
    this.initializeFormGroup();
    this.dialogRef.close();
  }
  get errorControl() {
    return this.submitForm.controls;
  }
  getDate(e) {
    let date = new Date(e.target.value).toISOString().substring(0, 10);
    this.submitForm.get('datenaisse').setValue(date, {
      onlyself: true
    })
  }
  onSubmit() {
    this.isSubmitted = true;
    if (!this.submitForm.valid) {
      console.log('validation erreur');
      } else {

        this.service.addUtilisateur(this.submitForm.value).subscribe(

          res => {
            console.log(res);
          },
          err => {
            console.log(err);
          }
        );
        this.notificationService.success('creation reusssie');
      /* this.service.update(this.submitForm.value); */
        this.submitForm.reset();
        this.initializeFormGroup();
        this.notificationService.success(':: Submitted successfully');
        this.onClose();

    }
  }

}
