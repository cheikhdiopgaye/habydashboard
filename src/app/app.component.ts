import { AthenticationService } from './athentication.service';
import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from './services/utilisateurSservice/utilisateur.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Angularprojet';
  userconnecte:any;

  constructor(private authservice: AthenticationService,
            private clientservice: UtilisateurService) {}
  ngOnInit(): void {
    this.authservice.loadToken();
    this.clientservice.getUserconnnecte().subscribe(
      res => {
        this.userconnecte = res;
        console.log(res);
      },
      err => {console.log(err);}
    );
  }
  LogOut() {
    this.authservice.LogOut();
  }
}
