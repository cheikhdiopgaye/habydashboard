import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { MatDialog, MatDialogConfig, MatSort} from "@angular/material";
import { NotificationsService } from '../services/notifications.service';
import { NewclientComponent } from '../newclient/newclient.component';
import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';
import { AthenticationService } from '../athentication.service';


@Component({
  selector: 'app-listclient',
  templateUrl: './listclient.component.html',
  styleUrls: ['./listclient.component.css']
})
export class ListclientComponent implements OnInit {


  partenaires: any;
  dataSource : any;
  searchKey: string;
  userconnecte:any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private clientservice: UtilisateurService,
              private router: Router,
              private dialog: MatDialog,
              private authservice: AthenticationService,
              ) { }

  ngOnInit() {
    this.clientservice.getUserconnnecte().subscribe(
      res => {
        this.userconnecte = res
        console.log(res);
      },
      err => {
          console.log(err);
      }
    );
    this.clientservice.getListuser().subscribe(
        res => {
          this.partenaires = res
          console.log(res);
          this.dataSource = new MatTableDataSource(this.partenaires);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.dataSource.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
            });
          };
        },
        err => {
              console.log(this.partenaires)
              if (err instanceof HttpErrorResponse){
                if(err.status === 401){
                  this.router.navigate(['/login'])
                }
              }
        }
      )
  }
  displayedColumns: string[] = ['datecreation','numerocompte','clients.nomcomplet', 'clients.adresse', 'clients.mobile', 'solde','clients.user.prenom','clients.user.nom','clients.user.telephone','actions'];
  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }
  onSearchClear(){
    this.searchKey = "";
    this.applyFilter();
  }
}
