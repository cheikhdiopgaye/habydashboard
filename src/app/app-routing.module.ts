import { ListedepotsComponent } from './listedepots/listedepots.component';
import { Accueil1Component } from './accueil1/accueil1.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { PartenairesComponent } from './partenaires/partenaires.component';
import { ListeuserComponent } from './listeuser/listeuser.component';
import { ListetransfertComponent } from './listetransfert/listetransfert.component';
import { ListeretraituserComponent } from './listeretraituser/listeretraituser.component';
import { ListclientComponent } from './listclient/listclient.component';
import { ListadminComponent } from './listadmin/listadmin.component';
import { NewclientComponent } from './newclient/newclient.component';
import { AuthGuard } from './services/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },

  {
    path: 'login',

    component : LoginComponent
  },
  {
    path: 'Entreprise',canActivate:[AuthGuard],
    component : PartenairesComponent
  },
  {path:'newclient', component: NewclientComponent},
  {path: 'listedepots',canActivate:[AuthGuard] ,component: ListedepotsComponent},
  {path: 'listeusers', canActivate:[AuthGuard],component: ListeuserComponent},
  {path: 'listetransfertu',canActivate:[AuthGuard] ,component: ListetransfertComponent},
  {path: 'accueil1',canActivate:[AuthGuard],component:Accueil1Component},
  {path: 'listeretraituser',canActivate:[AuthGuard] ,component:ListeretraituserComponent},
  {path: 'listclients', canActivate:[AuthGuard],component:ListclientComponent},
  {path: 'listadmin',canActivate:[AuthGuard] ,component: ListadminComponent}





];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
