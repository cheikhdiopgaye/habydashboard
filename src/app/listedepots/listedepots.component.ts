import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import { MatDialog, MatDialogConfig, MatSort} from "@angular/material";
import { NotificationsService } from '../services/notifications.service';
import { NewpaiementComponent } from '../newpaiement/newpaiement.component';
import { OperationsService } from '../services/operations.service';
import { AthenticationService } from '../athentication.service';
import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';


@Component({
  selector: 'app-listedepots',
  templateUrl: './listedepots.component.html',
  styleUrls: ['./listedepots.component.css']
})
export class ListedepotsComponent implements OnInit {

  listuser :any;
  dataSource :any;
  searchKey: string;
  userconnecte:any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort; MatPaginator;
  constructor(private operationService: OperationsService,
              private router: Router,
              private dialog: MatDialog,
              private notificationService: NotificationsService,
              private authservice: AthenticationService,
              private userService: UtilisateurService) { }

  ngOnInit() {
    this.userService.getUserconnnecte().subscribe(
      res => {
        this.userconnecte = res
        console.log(res);
      },
      err => {
          console.log(err);
      }
    );
    this.operationService.PaiementList().subscribe(

        res => {
          this.listuser = res
          this.dataSource = new MatTableDataSource(this.listuser);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.dataSource.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
            });
          };
        },
        err => {
                console.log(this.listuser)
                if (err instanceof HttpErrorResponse){
                  if(err.status === 401){
                    this.router.navigate(['/login'])
                  }
                }
        }
      )
  }
  displayedColumns: string[] = ['dateoperation','numoperation','montant', 'compte.clients.nomcomplet','actions'];

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onCreate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "50%";
    this.dialog.open(NewpaiementComponent, dialogConfig);
  }

}
