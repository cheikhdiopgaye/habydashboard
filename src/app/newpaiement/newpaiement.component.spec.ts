import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewpaiementComponent } from './newpaiement.component';

describe('NewpaiementComponent', () => {
  let component: NewpaiementComponent;
  let fixture: ComponentFixture<NewpaiementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewpaiementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewpaiementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
