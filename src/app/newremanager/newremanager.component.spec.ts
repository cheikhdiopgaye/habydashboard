import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewremanagerComponent } from './newremanager.component';

describe('NewremanagerComponent', () => {
  let component: NewremanagerComponent;
  let fixture: ComponentFixture<NewremanagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewremanagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewremanagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
