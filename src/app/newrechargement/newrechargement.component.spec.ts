import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewrechargementComponent } from './newrechargement.component';

describe('NewrechargementComponent', () => {
  let component: NewrechargementComponent;
  let fixture: ComponentFixture<NewrechargementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewrechargementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewrechargementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
