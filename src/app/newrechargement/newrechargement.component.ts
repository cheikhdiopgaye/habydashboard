import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { NotificationsService } from '../services/notifications.service';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NewremanagerComponent } from '../newremanager/newremanager.component';
import { OperationsService } from '../services/operations.service';

@Component({
  selector: 'app-newrechargement',
  templateUrl: './newrechargement.component.html',
  styleUrls: ['./newrechargement.component.css']
})
export class NewrechargementComponent implements OnInit {

  submitForm: FormGroup;
  isSubmitted = false;
  constructor(private operationService: OperationsService,
    public formBuilder: FormBuilder ,
    private notificationService: NotificationsService,
    public dialogRef: MatDialogRef<NewremanagerComponent>) { }

  ngOnInit() {
   /*  this.service.getEmployees(); */
   this.submitForm = this.formBuilder.group({
    numerocompte: ['', [Validators.required, Validators.minLength(5)]],
    montant: ['', [Validators.required, Validators.minLength(3)]]
  })
  }
  initializeFormGroup() {
    this.submitForm.setValue({
      numerocompte: '',
      montant: ''
    });
  }
  onClear() {
    this.submitForm.reset();
    this.initializeFormGroup()
  }

  onSubmit() {
    this.isSubmitted = true;
    if (!this.submitForm.valid) {
      console.log('validation erreur');
      } else {

        this.operationService.NouveauRechargement(this.submitForm.value).subscribe(

          res => {
            console.log(res);
          },
          err => {
            console.log(err);
          }
        );
        /* this.service.update(this.submitForm.value); */
        this.submitForm.reset();
        this.initializeFormGroup();
        this.notificationService.success(':: Submitted successfully');
    }
  }

  onClose() {
    this.submitForm.reset();
    this.initializeFormGroup();
    this.dialogRef.close();
  }
}
