import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PartenairesComponent } from './partenaires/partenaires.component';
import { Accueil1Component } from './accueil1/accueil1.component';
import { FooterComponent } from './footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListedepotsComponent } from './listedepots/listedepots.component';
import { ListeuserComponent } from './listeuser/listeuser.component';
import {MaterialModule} from './material/material.module';
import { ListetransfertComponent } from './listetransfert/listetransfert.component';

import { ToastrModule } from 'ngx-toastr';
import { ListeretraituserComponent } from './listeretraituser/listeretraituser.component';
import {AlertModule} from './schared';
import { AlertComponent } from './shared/modules/alert/alert.component';
import {AlertService} from './shared/modules/alert/services/alert.service';
import { MatIconModule } from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import { NewclientComponent } from './newclient/newclient.component';
import { NewadminComponent } from './newadmin/newadmin.component';
import { NewresponsableComponent } from './newresponsable/newresponsable.component';
import { NewremanagerComponent } from './newremanager/newremanager.component';
import { NewrechargementComponent } from './newrechargement/newrechargement.component';
import { NewpaiementComponent } from './newpaiement/newpaiement.component';
import { OperationsComponent } from './operations/operations.component';
import { ListclientComponent } from './listclient/listclient.component';
import { ListadminComponent } from './listadmin/listadmin.component';
import {MatButtonModule, MatCardModule, MatInputModule} from '@angular/material';
import { NavebarComponent } from './navebar/navebar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { TokenInterceptorService } from './services/token-interceptor.service';
import { AuthGuard } from './services/auth.guard';
import { EnterComponent } from './enter/enter.component';







@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PartenairesComponent,
    Accueil1Component,
    FooterComponent,
    ListedepotsComponent,
    ListeuserComponent,
    ListetransfertComponent,
    ListeretraituserComponent,
    AlertComponent,
    NewclientComponent,
    NewadminComponent,
    NewresponsableComponent,
    NewremanagerComponent,
    NewrechargementComponent,
    NewpaiementComponent,
    OperationsComponent,
    ListclientComponent,
    ListadminComponent,
    NavebarComponent,
    SidebarComponent,
    EnterComponent

  ],
  imports: [
  BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatIconModule,
    MatFormFieldModule,
    AlertModule,
    MatCardModule, MatButtonModule, MatInputModule,

    MatGridListModule,
    ToastrModule.forRoot()

  ],
  providers: [AlertService,
              AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  exports: [AlertComponent],
  entryComponents:[ NewclientComponent, NewrechargementComponent, NewpaiementComponent, NewadminComponent ]
})
export class AppModule { }
