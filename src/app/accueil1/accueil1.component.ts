import { Component, OnInit } from '@angular/core';
import { StatService } from '../services/stat.service';
import { AthenticationService } from '../athentication.service';
import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';

@Component({
  selector: 'app-accueil1',
  templateUrl: './accueil1.component.html',
  styleUrls: ['./accueil1.component.css']
})
export class Accueil1Component implements OnInit {
  NbreClient;
  NbrePaiement: any;
  NbreRecharg:  any;
  nbreoperation: any;
  userconnecte:any;

  constructor(private statistique: StatService,
    private authservice: AthenticationService,
    private userService: UtilisateurService) { }

  ngOnInit() {
     this.statistique.NbreClient().subscribe(
        res => {
          this.NbreClient = res
          console.log(res);
        },
        err => {
            console.log(err);
        }
      );
      this.statistique.Nbrepaiement().subscribe(
        res => {
          this.NbrePaiement = res
          console.log(res);
        },
        err => {
            console.log(err);
        }
      );
      this.statistique.NbreRechargement().subscribe(
        res => {
          this.NbreRecharg = res
          console.log(res);
        },
        err => {
            console.log(err);
        }
      );
      this.statistique.Nbroperation().subscribe(
        res => {
          this.nbreoperation = res
          console.log(res);
        },
        err => {
            console.log(err);
        }
      );
      this.userService.getUserconnnecte().subscribe(
        res => {
          this.userconnecte = res
          console.log(res);
        },
        err => {
            console.log(err);
        }
      );

  }

}
