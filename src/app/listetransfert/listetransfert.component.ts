
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialog, MatDialogConfig, MatSort} from "@angular/material";
import { NotificationsService } from '../services/notifications.service';
import { OperationsService } from '../services/operations.service';
import { AthenticationService } from '../athentication.service';
import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';


@Pipe({
    name: 'dateFormatPipe',
})
export class dateFormatPipe implements PipeTransform {
    transform(value: string) {
       var datePipe = new DatePipe("en-US");
        value = datePipe.transform(value, 'dd/MM/yyyy');
        return value;
    }
}

@Component({
  selector: 'app-listetransfert',
  templateUrl: './listetransfert.component.html',
  styleUrls: ['./listetransfert.component.css']
})
export class ListetransfertComponent implements OnInit {

  transfert :any;
  dataSource : any;
  searchKey: string;
  userconnecte:any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private operationService: OperationsService,
    private router: Router,
    private dialog: MatDialog,
    private notificationService: NotificationsService,
    private authservice: AthenticationService,
    private userService: UtilisateurService) { }

  ngOnInit() {
    this.userService.getUserconnnecte().subscribe(
      res => {
        this.userconnecte = res
        console.log(res);
      },
      err => {
          console.log(err);
      }
    );
    this.operationService.operationList()
      .subscribe(

        res => {
          this.transfert = res
          console.log(res);
          this.dataSource = new MatTableDataSource(this.transfert);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.dataSource.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
            });
          };
        },
        err => {
               console.log(this.transfert);
               if (err instanceof HttpErrorResponse) {
                  if (err.status === 401) {
                    this.router.navigate(['/login']);
                  }
                }
        }
      )
  }
  displayedColumns: string[] = ['dateoperation','numoperation','montant', 'typeofoperation', 'compte.clients.nomcomplet','user.prenom','user.nom','user.telephone','actions'];


  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }

}
