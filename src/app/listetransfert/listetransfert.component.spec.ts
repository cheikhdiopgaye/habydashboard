import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListetransfertComponent } from './listetransfert.component';

describe('ListetransfertComponent', () => {
  let component: ListetransfertComponent;
  let fixture: ComponentFixture<ListetransfertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListetransfertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListetransfertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
