
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialog, MatDialogConfig, MatSort} from "@angular/material";
import { NotificationsService } from '../services/notifications.service';

import { UtilisateurService } from '../services/utilisateurSservice/utilisateur.service';
import { NewadminComponent } from '../newadmin/newadmin.component';
import { AthenticationService } from '../athentication.service';

export class dateFormatPipe implements PipeTransform {
  transform(value: string) {
     var datePipe = new DatePipe("en-US");
      value = datePipe.transform(value, 'dd/MM/yyyy');
      return value;
  }
}


@Component({
  selector: 'app-listeretraituser',
  templateUrl: './listeretraituser.component.html',
  styleUrls: ['./listeretraituser.component.css']
})
export class ListeretraituserComponent implements OnInit {

  listeretrait :any;
  dataSource : any;
  searchKey: string;
  userconnecte:any;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private service: UtilisateurService,
              private router: Router,
              private dialog: MatDialog,
              private notificationService: NotificationsService,
              private authservice: AthenticationService) { }

  ngOnInit() {
    this.service.getUserconnnecte().subscribe(
      res => {
        this.userconnecte = res
        console.log(res);
      },
      err => {
          console.log(err);
      }
    );
    this.service.getListadmin()
      .subscribe(

        res => {
          this. listeretrait = res
          console.log(res);
          this.dataSource = new MatTableDataSource(this. listeretrait);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.dataSource.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
            });
          };
        },
        err => {
               console.log(this. listeretrait)
            if (err instanceof HttpErrorResponse){
              if(err.status === 401){
                this.router.navigate(['/login'])
              }
            }
        }
      )
  }
  displayedColumns: string[] = ['photo','prenom','nom',  'profil','telephone','username',  'actions'];

  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onCreate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(NewadminComponent, dialogConfig);
  }

}
