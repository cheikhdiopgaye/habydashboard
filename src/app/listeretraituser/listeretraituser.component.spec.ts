import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeretraituserComponent } from './listeretraituser.component';

describe('ListeretraituserComponent', () => {
  let component: ListeretraituserComponent;
  let fixture: ComponentFixture<ListeretraituserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeretraituserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeretraituserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
