import { Component, OnInit } from '@angular/core';
import { AthenticationService } from '../athentication.service';

@Component({
  selector: 'app-navebar',
  templateUrl: './navebar.component.html',
  styleUrls: ['./navebar.component.css']
})
export class NavebarComponent implements OnInit {

  constructor(private authservice: AthenticationService) { }

  ngOnInit() {
  }
  LogOut() {
    this.authservice.LogOut();
  }

}
