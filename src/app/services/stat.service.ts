import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AthenticationService } from '../athentication.service';

@Injectable({
  providedIn: 'root'
})
export class StatService {

  constructor(private http: HttpClient, private authService: AthenticationService) { }

  NbreClient(): Observable<any> {
      var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
      const NbreclientUrl = "http://localhost:8000/api/security/lister/nombreclient";
      return this.http.get<any>(NbreclientUrl, {headers:headers})
  }

  Nbrepaiement(): Observable<any> {
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const NbrepaiementUrl = "http://localhost:8000/api/security/lister/paiements";
    return this.http.get<any>(NbrepaiementUrl, {headers:headers})
  }

  NbreRechargement(): Observable<any> {
    var NbreRechUrl = 'http://localhost:8000/api/security/lister/crediter';
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return this.http.get<any>(NbreRechUrl, {headers:headers})
  }

  Nbroperation(): Observable<any> {
    const NbreOperationUrl = 'http://localhost:8000/api/security/lister/nombreoperation';
    var headers= new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.http.get<any>(NbreOperationUrl, {headers:headers})
  }
}
