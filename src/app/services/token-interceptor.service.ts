import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AthenticationService } from '../athentication.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private injector:Injector) { }
  intercept(req,next) {
    let authService = this.injector.get(AthenticationService)
    let tokenizedReq=req.clone({
      setHeaders:{
        Authorization: `Bearer ${authService.jwt}`
      }
    })
    return next.handle(tokenizedReq);
  }
}
