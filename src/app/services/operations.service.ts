import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AthenticationService } from '../athentication.service';


@Injectable({
  providedIn: 'root'
})
export class OperationsService {
  constructor(private http: HttpClient, private authService: AthenticationService) { }

  nounveauPaiement(submit) {
      var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
      const paiementUrl = "http://localhost:8000/api/security/debiter";
      return this.http.post(paiementUrl, submit, {headers:headers})
  }

  NouveauRechargement(submit) {
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const rechargementUrl = "http://localhost:8000/api/security/operation";
    return this.http.post(rechargementUrl, submit, {headers:headers})
  }

  PaiementList(): Observable<any> {
    var RechargementListUrl = 'http://localhost:8000/api/security/lister/Paiement';
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return this.http.get<any>(RechargementListUrl, {headers:headers})
  }

  RechargementList(): Observable<any> {
    const paiementListUrl = 'http://localhost:8000/api/security/lister/Rechargement';
    var headers= new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.http.get<any>(paiementListUrl, {headers:headers})
  }
  operationList(): Observable<any> {
    const operationListUrl = 'http://localhost:8000/api/security/lister/operation';
    var headers= new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    return this.http.get<any>(operationListUrl, {headers:headers})
  }
}
