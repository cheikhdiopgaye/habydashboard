import { Injectable } from '@angular/core';
import { AthenticationService } from './../../athentication.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UtilisateurService {


  NewAdmin(submit) {
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const newadminUrl = "http://localhost:8000/api/inscription";
    return this.http.post(newadminUrl, submit, {headers:headers})
  }
  constructor(private http: HttpClient, private authService: AthenticationService) { }
  addUtilisateur(submit) {
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    const addUtilisateurUrl = "http://localhost:8000/api/security/client";
    return this.http.post(addUtilisateurUrl, submit, {headers:headers})
  }
  public listuserUrl = "http://localhost:8000/api/security/lister/compte";
  getListuser(): Observable<any> {

    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return this.http.get<any>(this.listuserUrl, {headers:headers})
  }

  public listadminUrl = "http://localhost:8000/api/security/lister/users";
  getListadmin(): Observable<any> {

    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return this.http.get<any>(this.listadminUrl, {headers:headers})
  }

  getUserconnnecte(): Observable<any> {
    const userConnecteUrl = "http://localhost:8000/api/security/lister/userconnecte";
    var headers= new HttpHeaders().set("Authorization", "Bearer " + localStorage.getItem('token'));
    return this.http.get<any>(userConnecteUrl, {headers:headers})
  }

}
