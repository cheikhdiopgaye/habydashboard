import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AthenticationService } from '../athentication.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard  implements CanActivate  {
  constructor(private router: Router, private authService: AthenticationService) {
  }
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
  if (!this.authService.isAuthenticated) {
  this.router.navigateByUrl("/login");
  }
  return true;
  }
}
