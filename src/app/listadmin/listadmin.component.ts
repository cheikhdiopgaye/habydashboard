
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog, MatDialogConfig, MatSort} from "@angular/material";
import { NewadminComponent } from '../newadmin/newadmin.component';
import { OperationsService } from '../services/operations.service';

@Component({
  selector: 'app-listadmin',
  templateUrl: './listadmin.component.html',
  styleUrls: ['./listadmin.component.css']
})
export class ListadminComponent implements OnInit {

  transfert :any;
  dataSource : any;
  searchKey: string;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  constructor(private transfertService: OperationsService,
    private router: Router,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.transfertService.RechargementList()
      .subscribe(
        res => {
          this.transfert = res
          console.log(res);
          this.dataSource = new MatTableDataSource(this.transfert);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
          this.dataSource.filterPredicate = (data, filter) => {
            return this.displayedColumns.some(ele => {
              return ele != 'actions' && data[ele].toLowerCase().indexOf(filter) != -1;
            });
          };
        },
        err => {
               console.log(this.transfert);
               if (err instanceof HttpErrorResponse) {
                  if (err.status === 401) {
                    this.router.navigate(['/login']);
                  }
                }
        }
      )
  }
  displayedColumns: string[] = ['numerotrans', 'montanttrans', 'datedenvoie', 'delairetrait', 'frais', 'commenvoyeur'];


  applyFilter() {
    this.dataSource.filter = this.searchKey.trim().toLowerCase();
  }
  onSearchClear() {
    this.searchKey = "";
    this.applyFilter();
  }
  onCreate() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = "60%";
    this.dialog.open(NewadminComponent, dialogConfig);
  }
}
